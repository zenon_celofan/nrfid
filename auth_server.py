from bottle import route, request, run

authentication_table = [['100', '200', 'A7B8DBD9']]

@route('/auth')
def authentication_request():
    reader_id = request.query.reader_id
    door_id = request.query.door_id
    card_id = request.query.card_id

    print("Authentication request received:")
    print("reader id: ", reader_id)
    print("door id", door_id)
    print("card id: ", card_id)

    for line in authentication_table:
        print(line[0])
        if line[0] == reader_id and line[1] == door_id and line[2] == card_id:
            print("Permission granted")
            return "Permission %granted%"
        else:
            print("Permission denied")
            return "Permission %denied%"

run(host="192.168.173.1", port = 80)