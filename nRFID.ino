#include <ESP8266WiFi.h>
#include <MFRC522.h>
#include <SPI.h>

#define READER_ID       100
#define DOOR_ID         200

const char* ssid     = "ER-AP";
const char* password = "whatisthetime";
const char* auth_server_ip = "192.168.173.1";
const int auth_server_port = 80;

String reader_id = String(READER_ID);
String door_id = String(DOOR_ID);
String card_id;

#define RST_PIN         D3
#define SS_PIN          D8
MFRC522 mfrc522(SS_PIN, RST_PIN);

#define LED_PIN   D4
int led_status = 0;

#define LOCK_PIN  D2

#define BUZZER_PIN  D1




void setup() {
  Serial.begin(9600);
  delay(10);

  SPI.begin();
  mfrc522.PCD_Reset();
  delay(100);
  mfrc522.PCD_Init();   // Init MFRC522

  wifi_connect();

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);   //turn_off LED

  pinMode(LOCK_PIN, INPUT);     //lock -> engage

  pinMode(BUZZER_PIN, OUTPUT);
  digitalWrite(BUZZER_PIN, LOW);   //silence
  
  Serial.println("INIT_END");
  beep();
}



void loop() {

  if (WiFi.status() != WL_CONNECTED) {
    wifi_connect();
  }

  if (mfrc522.PICC_IsNewCardPresent() == true) {
    if (mfrc522.PICC_ReadCardSerial() == true) {
      card_id= "";
      for (byte i = 0; i < mfrc522.uid.size; i++) {
         card_id.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : ""));
         card_id.concat(String(mfrc522.uid.uidByte[i], HEX));
      }
      card_id.toUpperCase();
      Serial.println("\nCard ID:" + card_id);
      
      if (ask_server(reader_id, door_id, card_id) == true) {
        led_blink();
        beep();
        open_lock();
      } else {
        led_blink();
        beep();
        led_blink();
        beep();
        led_blink();
        beep();
      }

      delay(1000);
      
    }
  }

}



void wifi_connect(void) {
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


bool ask_server(String reader_id, String door_id, String card_id) {
  String response = "";
  
  Serial.print("connecting to auth server ");
  Serial.println(auth_server_ip);

  WiFiClient client;

  if (!client.connect(auth_server_ip, auth_server_port)) {
    Serial.println("connection failed");
    return false;
  }
  
  String url_auth_query = "/auth?reader_id=" + reader_id + "&door_id=" + door_id + "&card_id=" + card_id;

  Serial.print("Requesting URL: ");
  Serial.println(url_auth_query);
  
  client.print(String("GET ") + url_auth_query + " HTTP/1.1\r\n" +
               "Host: " + auth_server_ip + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return false;
    }
  }
  
  while(client.available()){
    client.readStringUntil('%');
    response = client.readStringUntil('%');
    Serial.print(response);
  }
  
  Serial.println();
  Serial.println("closing connection");

  if (response == "granted") {
    return true;
  } else {
    return false;
  }
}


void open_lock(void) {
  pinMode(LOCK_PIN, OUTPUT); 
  digitalWrite(LOCK_PIN, LOW);
  delay(1000);
  pinMode(LOCK_PIN, INPUT);
}


void led_blink(void) {
  digitalWrite(LED_PIN, HIGH);
  delay(200);
  digitalWrite(LED_PIN, LOW);
  delay(200);
}


void beep(void) {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(20);
  digitalWrite(BUZZER_PIN, LOW);
}

