Zasada działania:
    
	Każdy czytnik posiada unikalny numer ID.
	Każdy zamek posiada unikalny numer ID.
	Oba numery są wpisane na stałe w firmware'rze czytnika (READER_ID, LOCK_ID)
	
	Czytnik loguje się do sieci wifi ("ER-AP", "whatisthetime") w której znajduje się serwer autoryzacji (192.168.173.1:80).
	
	Po przyłożeniu karty do czytnika, wysyła on zapytanie ULR do serwera, o składni:
	
	    "GET /auth?reader_id=999&door_id=999&card_id=999999"
		
	Następnie przez 5s czeka na odpowiedź.
	Jeśli w ospowiedzi znajduje się ciąg znaków "%granted%" elektrozamek zostaje odblokowany.